# translation of kdevgrepview.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2013.
# Xosé <xosecalvo@gmail.com>, 2012, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2013, 2015, 2016, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdevgrepview\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-29 00:44+0000\n"
"PO-Revision-Date: 2019-07-13 12:31+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: grepdialog.cpp:52
#, fuzzy, kde-format
#| msgid "All Open Files"
msgctxt "@item:inlistbox"
msgid "All Open Files"
msgstr "Todos os ficheiros abertos"

#: grepdialog.cpp:53
#, fuzzy, kde-format
#| msgid "All Open Projects"
msgctxt "@item:inlistbox"
msgid "All Open Projects"
msgstr "Todos os proxectos abertos"

#: grepdialog.cpp:181 grepdialog.cpp:520 grepviewplugin.cpp:91
#, fuzzy, kde-format
#| msgid "Find/Replace in Files"
msgctxt "@title:window"
msgid "Find/Replace in Files"
msgstr "Atopar ou substituír en ficheiros"

#: grepdialog.cpp:189
#, kde-format
msgctxt "@action:button"
msgid "Search..."
msgstr "Buscar…"

#: grepdialog.cpp:256
#, fuzzy, kde-format
#| msgctxt "@title:window"
#| msgid "Select directory to search in"
msgctxt "@title:window"
msgid "Select Directory to Search in"
msgstr "Seleccione o directorio onde buscar"

#: grepdialog.cpp:512
#, kde-format
msgid "%2, and %1 more item"
msgid_plural "%2, and %1 more items"
msgstr[0] "%2 e %1 elemento máis"
msgstr[1] "%2 e outros %1 elementos"

#: grepdialog.cpp:525
#, fuzzy, kde-format
#| msgid "Search \"%1\" in %2"
msgctxt "@item search result"
msgid "Search \"%1\" in %2"
msgstr "Buscar «%1» en %2"

#: grepdialog.cpp:530
#, fuzzy, kde-format
#| msgid "Search \"%1\" in %2 (at time %3)"
msgctxt "@item search result"
msgid "Search \"%1\" in %2 (at time %3)"
msgstr "Buscar «%1» en %2 (na hora %3)"

#: grepjob.cpp:97
#, kde-format
msgid "Find in Files"
msgstr "Atopar en ficheiros"

#: grepjob.cpp:113 grepjob.cpp:216
#, kde-format
msgid "Search aborted"
msgstr "Interrompeuse a busca"

#: grepjob.cpp:122
#, kde-format
msgid "No files found matching the wildcard patterns"
msgstr "Non se atopou ningún ficheiro que coincidise cos padróns de comodíns"

#: grepjob.cpp:140
#, kde-format
msgctxt ""
"Capture is the text which is \"captured\" with () in regular expressions see "
"https://doc.qt.io/qt-5/qregexp.html#capturedTexts"
msgid "Captures are not allowed in pattern string"
msgstr "Non se permiten capturas na cadea dos padróns"

#: grepjob.cpp:161
#, kde-format
msgid "Searching for <b>%2</b> in one file"
msgid_plural "Searching for <b>%2</b> in %1 files"
msgstr[0] "Estase a buscar <b>%2</b> nun ficheiro"
msgstr[1] "Estase a buscar <b>%2</b> en %1 ficheiros"

#: grepjob.cpp:182
#, kde-format
msgid "Collecting files..."
msgstr "Estanse a recopilar os ficheiros…"

#: grepjob.cpp:260
#, kde-format
msgid "Failed: %1"
msgstr "Fallou: %1"

#: grepjob.cpp:263
#, kde-format
msgid "No results found"
msgstr "Non se achou ningún resultado"

#: grepjob.cpp:282
#, kde-format
msgid "Grep: %1"
msgstr "Grep: %1"

#: grepoutputdelegate.cpp:73 grepoutputdelegate.cpp:140
#, kde-format
msgid "Line %1: "
msgstr "Liña %1:"

#: grepoutputmodel.cpp:377
#, kde-format
msgid "<b>1</b> match"
msgid_plural "<b>%1</b> matches"
msgstr[0] "<b>1</b> coincidencia"
msgstr[1] "<b>%1</b> coincidencias"

#: grepoutputmodel.cpp:378
#, kde-format
msgid "<b>1</b> file"
msgid_plural "<b>%1</b> files"
msgstr[0] "<b>1</b> ficheiro"
msgstr[1] "<b>%1</b> ficheiros"

#: grepoutputmodel.cpp:380
#, kde-format
msgctxt "%1 is e.g. '4 matches', %2 is e.g. '1 file'"
msgid "<b>%1 in %2</b>"
msgstr "<b>%1 en %2</b>"

#: grepoutputmodel.cpp:382
#, kde-format
msgid "%2: 1 match"
msgid_plural "%2: %1 matches"
msgstr[0] "%2: %1 coincidencia"
msgstr[1] "%2: %1 coincidencias"

#: grepoutputmodel.cpp:452
#, kde-format
msgctxt ""
"%1 is the old text, %2 is the new text, %3 is the file path, %4 and %5 are "
"its row and column"
msgid "Failed to replace <b>%1</b> by <b>%2</b> in %3:%4:%5"
msgstr "Fallou a substitución de <b>%1</b> por <b>%2</b> en %3:%4:%5"

#: grepoutputview.cpp:71
#, kde-format
msgctxt "@title:window"
msgid "Find/Replace Output View"
msgstr "Atopar ou substituír a vista da saída"

#: grepoutputview.cpp:74
#, fuzzy, kde-format
#| msgid "&Previous Item"
msgctxt "@action"
msgid "&Previous Item"
msgstr "Elemento &anterior"

#: grepoutputview.cpp:75
#, fuzzy, kde-format
#| msgid "&Next Item"
msgctxt "@action"
msgid "&Next Item"
msgstr "Elemento &seguinte"

#: grepoutputview.cpp:80
#, fuzzy, kde-format
#| msgid "C&ollapse All"
msgctxt "@action"
msgid "C&ollapse All"
msgstr "C&ontraer todo"

#: grepoutputview.cpp:82
#, fuzzy, kde-format
#| msgid "&Expand All"
msgctxt "@action"
msgid "&Expand All"
msgstr "&Expandir todo"

#: grepoutputview.cpp:86
#, fuzzy, kde-format
#| msgid "New &Search"
msgctxt "@action"
msgid "New &Search"
msgstr "Nova &busca"

#: grepoutputview.cpp:87
#, fuzzy, kde-format
#| msgid "Refresh"
msgctxt "@action"
msgid "Refresh"
msgstr "Actualizar"

#: grepoutputview.cpp:89
#, fuzzy, kde-format
#| msgid "Clear Search History"
msgctxt "@action"
msgid "Clear Search History"
msgstr "Limpar o historial de buscas"

#: grepoutputview.cpp:323
#, kde-format
msgid "Do you want to replace with an empty string?"
msgstr "Quere substituír por unha cadea baleira?"

#: grepoutputview.cpp:324
#, fuzzy, kde-format
#| msgid "Start replacement"
msgctxt "@title:window"
msgid "Start Replacement"
msgstr "Comezar a substitución"

#: grepoutputview.cpp:325
#, fuzzy, kde-format
#| msgid "&Replace"
msgctxt "@action:button"
msgid "Replace"
msgstr "&Substituír"

#. i18n: ectx: property (text), widget (QLabel, replacementLabel)
#: grepoutputview.ui:41
#, fuzzy, kde-format
#| msgid "Replacement &text:"
msgctxt "@label:listbox"
msgid "Replacement &text:"
msgstr "&Texto co que substituír:"

#. i18n: ectx: property (toolTip), widget (KComboBox, replacementCombo)
#: grepoutputview.ui:57
#, fuzzy, kde-format
#| msgid "Enter the replacement pattern."
msgctxt "@info:tooltip"
msgid "Enter the replacement pattern"
msgstr "Introduza o padrón de substitución."

#. i18n: ectx: property (toolTip), widget (QPushButton, applyButton)
#: grepoutputview.ui:76
#, fuzzy, kde-format
#| msgid "Apply replacement on selected items."
msgctxt "@info:tooltip"
msgid "Apply replacement on selected items"
msgstr "Aplicar as substitucións aos elementos escollidos."

#. i18n: ectx: property (text), widget (QPushButton, applyButton)
#: grepoutputview.ui:79
#, fuzzy, kde-format
#| msgid "&Replace"
msgctxt "@action:button"
msgid "&Replace"
msgstr "&Substituír"

#: grepviewplugin.cpp:76
#, fuzzy, kde-format
#| msgid "Find/Replace in Fi&les..."
msgctxt "@action"
msgid "Find/Replace in Fi&les..."
msgstr "Atopar ou substituír en fic&heiros…"

#: grepviewplugin.cpp:79
#, fuzzy, kde-format
#| msgid "Search for expressions over several files"
msgctxt "@info:tooltip"
msgid "Search for expressions over several files"
msgstr "Buscar expresións regulares en varios ficheiros"

#: grepviewplugin.cpp:81
#, fuzzy, kde-format
#| msgid ""
#| "Opens the 'Find/Replace in files' dialog. There you can enter a regular "
#| "expression which is then searched for within all files in the directories "
#| "you specify. Matches will be displayed, you can switch to a match "
#| "directly. You can also do replacement."
msgctxt "@info:whatsthis"
msgid ""
"Opens the 'Find/Replace in Files' dialog. There you can enter a regular "
"expression which is then searched for within all files in the directories "
"you specify. Matches will be displayed, you can switch to a match directly. "
"You can also do replacement."
msgstr ""
"Abre o diálogo de «Buscar ou substituír en ficheiros». Desde o diálogo "
"poderá inserir unha expresión regular, e buscarase en todos os ficheiros dos "
"directorios que especifique. Mostraranse as coincidencias que se atopen, e "
"poderá ir a calquera delas directamente. Tamén se poden realizar "
"substitucións."

#: grepviewplugin.cpp:129 grepviewplugin.cpp:152
#, fuzzy, kde-format
#| msgid "Find/Replace in This Folder..."
msgctxt "@action:inmenu"
msgid "Find/Replace in This Folder..."
msgstr "Atopar ou substituír neste cartafol…"

#: grepviewplugin.cpp:140
#, fuzzy, kde-format
#| msgid "&Find/Replace in Files..."
msgctxt "@action:inmenu"
msgid "&Find/Replace in Files..."
msgstr "Atopar e substituír en &ficheiros…"

#. i18n: ectx: property (toolTip), widget (QComboBox, patternCombo)
#: grepwidget.ui:35
#, fuzzy, kde-format
#| msgid "Enter the regular expression you want to search for here."
msgctxt "@info:tooltip"
msgid "Enter the regular expression you want to search for"
msgstr "Introduza aquí a expresión regular que quere buscar."

#. i18n: ectx: property (whatsThis), widget (QComboBox, patternCombo)
#: grepwidget.ui:59
#, fuzzy, kde-format
#| msgid ""
#| "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#| "REC-html40/strict.dtd\">\n"
#| "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#| "css\">\n"
#| "p, li { white-space: pre-wrap; }\n"
#| "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#| "font-weight:400; font-style:normal;\">\n"
#| "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">Enter the regular "
#| "expression you want to search for here.</p>\n"
#| "<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">If you do not check "
#| "\"Regular Expression\" below, this is considered a raw string. That "
#| "means, all meta characters are escaped.</p>\n"
#| "<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">Possible meta "
#| "characters are:</p>\n"
#| "<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
#| "right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
#| "bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
#| "indent:0px;\"><span style=\" font-weight:600;\">.</span> - Matches any "
#| "character</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">^</span> - Matches the beginning of a line</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">$</span> - Matches the end of a line</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">\\b</span> - Matches a word boundary</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">\\s</span> - Matches any whitespace character</li></ul>\n"
#| "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">The following "
#| "repetition operators exist:</p>\n"
#| "<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
#| "right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
#| "bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
#| "indent:0px;\"><span style=\" font-weight:600;\">?</span> - The preceding "
#| "item is matched at most once</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">*</span> - The preceding item is matched zero or more "
#| "times</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">+</span> - The preceding item is matched one or more times</"
#| "li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
#| "\">n</span><span style=\" font-weight:600;\">}</span> - The preceding "
#| "item is matched exactly <span style=\" font-style:italic;\">n</span> "
#| "times</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
#| "\">n</span><span style=\" font-weight:600;\">,}</span> - The preceding "
#| "item is matched <span style=\" font-style:italic;\">n</span> or more "
#| "times</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">{,</span><span style=\" font-weight:600; font-style:italic;"
#| "\">n</span><span style=\" font-weight:600;\">}</span> - The preceding "
#| "item is matched at most <span style=\" font-style:italic;\">n</span> "
#| "times</li>\n"
#| "<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#| "weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
#| "\">n</span><span style=\" font-weight:600;\">,</span><span style=\" font-"
#| "weight:600; font-style:italic;\">m</span><span style=\" font-weight:600;"
#| "\">}</span> - The preceding item is matched at least <span style=\" font-"
#| "style:italic;\">n</span>, but at most <span style=\" font-style:italic;"
#| "\">m</span> times.</li></ul>\n"
#| "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">Furthermore, "
#| "backreferences to bracketed subexpressions are available via the notation "
#| "\\<span style=\" font-style:italic;\">n</span>.</p>\n"
#| "<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
#| "right:0px; -qt-block-indent:0; text-indent:0px;\">For further reference, "
#| "look at <a href=\"https://www.pcre.org/\"><span style=\" text-decoration: "
#| "underline; color:#0057ae;\">www.pcre.org</span></a> or <span style=\" "
#| "font-style:italic;\">man pcresyntax.</span></p></body></html>"
msgctxt "@info:whatsthis"
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Enter the regular "
"expression you want to search for here.</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">If you do not check "
"\"Regular Expression\" below, this is considered a raw string. That means, "
"all meta characters are escaped.</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Possible meta characters "
"are:</p>\n"
"<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
"right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
"bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
"indent:0px;\"><span style=\" font-weight:600;\">.</span> - Matches any "
"character</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">^</span> - Matches the beginning of a line</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">$</span> - Matches the end of a line</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">\\b</span> - Matches a word boundary</li>\n"
"<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">\\s</span> - Matches any whitespace character</li></ul>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">The following repetition "
"operators exist:</p>\n"
"<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
"right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
"bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
"indent:0px;\"><span style=\" font-weight:600;\">?</span> - The preceding "
"item is matched at most once</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">*</span> - The preceding item is matched zero or more times</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">+</span> - The preceding item is matched one or more times</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">}</span> - The preceding item "
"is matched exactly <span style=\" font-style:italic;\">n</span> times</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">,}</span> - The preceding item "
"is matched <span style=\" font-style:italic;\">n</span> or more times</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{,</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">}</span> - The preceding item "
"is matched at most <span style=\" font-style:italic;\">n</span> times</li>\n"
"<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">,</span><span style=\" font-"
"weight:600; font-style:italic;\">m</span><span style=\" font-weight:600;\">}"
"</span> - The preceding item is matched at least <span style=\" font-style:"
"italic;\">n</span>, but at most <span style=\" font-style:italic;\">m</span> "
"times.</li></ul>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Furthermore, "
"backreferences to bracketed subexpressions are available via the notation "
"\\<span style=\" font-style:italic;\">n</span>.</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">For further reference, "
"look at <a href=\"https://www.pcre.org/\"><span style=\" text-decoration: "
"underline; color:#0057ae;\">www.pcre.org</span></a> or <span style=\" font-"
"style:italic;\">man pcresyntax.</span></p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Insira aquí a expresión "
"regular que quere buscar.</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Se non marca «Expresión "
"regular» abaixo, considérase que é unha cadea crúa. Isto significa que se "
"escapan todos os meta-caracteres..</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Os meta-caracteres "
"posíbeis son:</p>\n"
"<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
"right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
"bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
"indent:0px;\"><span style=\" font-weight:600;\">.</span> - Coincide con "
"calquera carácter</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">^</span> - Coincide co comezo dunha liña</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">$</span> - Coincide co final dunha liña</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">\\b</span> - Coincide cun límite de palabra</li>\n"
"<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">\\s</span> - Coincide con calquera espazo en branco</li></ul>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Existen os operadores de "
"repetición seguintes:</p>\n"
"<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-"
"right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-"
"bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
"indent:0px;\"><span style=\" font-weight:600;\">?</span> - O elemento "
"anterior coincide polo menos unha vez</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">*</span> - O elemento anterior coincide cero ou máis veces</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">+</span> - O elemento anterior coincide unha ou máis veces</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">}</span> - O elmento anterior "
"coincide exactamente <span style=\" font-style:italic;\">n</span> veces</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">,}</span> - O elemento anterior "
"coincide <span style=\" font-style:italic;\">n</span> ou máis veces</li>\n"
"<li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{,</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">}</span> - O elemento anterior "
"coincide como máximo <span style=\" font-style:italic;\">n</span> veces</"
"li>\n"
"<li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">{</span><span style=\" font-weight:600; font-style:italic;"
"\">n</span><span style=\" font-weight:600;\">,</span><span style=\" font-"
"weight:600; font-style:italic;\">m</span><span style=\" font-weight:600;\">}"
"</span> - O elemento anterior coincide cando menos <span style=\" font-style:"
"italic;\">n</span>, pero como máximo <span style=\" font-style:italic;\">m</"
"span> veces.</li></ul>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Ademais hai referencias "
"anteriores a subexpresións entre paréntese mediante a notación \\<span style="
"\" font-style:italic;\">n</span>.</p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Para máis referencia, "
"consulte <a href=\"https://www.pcre.org\"><span style=\" text-decoration: "
"underline; color:#0057ae;\">www.pcre.org</span></a> ou <span style=\" font-"
"style:italic;\">man pcresyntax.</span></p></body></html>"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: grepwidget.ui:69
#, fuzzy, kde-format
#| msgid "Template:"
msgctxt "@label:listbox"
msgid "Template:"
msgstr "Modelo:"

#. i18n: ectx: property (toolTip), widget (KComboBox, templateEdit)
#: grepwidget.ui:82
#, fuzzy, kde-format
#| msgid "This is the regular expression template."
msgctxt "@info:tooltip"
msgid "This is the regular expression template."
msgstr "Este é o modelo de expresións regulares."

#. i18n: ectx: property (whatsThis), widget (KComboBox, templateEdit)
#: grepwidget.ui:85
#, fuzzy, no-c-format, kde-format
#| msgid ""
#| "This is the regular expression template. <i>%s</i> will be replaced by "
#| "the pattern, while <i>%%</i> will be replaced by <i>%</i>."
msgctxt "@info:whatsthis"
msgid ""
"This is the regular expression template. <i>%s</i> will be replaced by the "
"pattern, while <i>%%</i> will be replaced by <i>%</i>."
msgstr ""
"Este é o modelo de expresións regulares. <i>%s</i> substitúese polo padrón, "
"mentres que <i>%%</i> substitúese por <i>%</i>."

#. i18n: ectx: property (toolTip), widget (QPushButton, syncButton)
#: grepwidget.ui:92
#, fuzzy, kde-format
#| msgid "Quickly select location from a set of directories"
msgctxt "@info:tooltip"
msgid "Quickly select location from a set of directories"
msgstr "Seleccionar rapidamente un directorio dunha lista."

#. i18n: ectx: property (text), widget (QPushButton, syncButton)
#: grepwidget.ui:95
#, fuzzy, kde-format
#| msgid "Quick Select"
msgctxt "@action:button"
msgid "Quick Select"
msgstr "Selección rápida"

#. i18n: ectx: property (text), widget (QLabel, depthLabel)
#: grepwidget.ui:102
#, fuzzy, kde-format
#| msgid "Depth:"
msgctxt "@label:spinbox"
msgid "Depth:"
msgstr "Profundidade:"

#. i18n: Full recursion will be used. For folder-based searches, that means searching in the target folder and all the subfolders, their subfolders, and so on.
#. i18n: ectx: property (specialValueText), widget (QSpinBox, depthSpin)
#: grepwidget.ui:118
#, fuzzy, kde-format
#| msgid "Full"
msgctxt "@item full recursion"
msgid "Full"
msgstr "Completa"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: grepwidget.ui:131
#, fuzzy, kde-format
#| msgid "Pattern:"
msgctxt "@label:listbox"
msgid "Pattern:"
msgstr "Padrón:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: grepwidget.ui:144
#, fuzzy, kde-format
#| msgid "Files:"
msgctxt "@label:listbox"
msgid "Files:"
msgstr "Ficheiros:"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: grepwidget.ui:157
#, fuzzy, kde-format
#| msgid "Case sensitive:"
msgctxt "@option:check"
msgid "Case sensitive:"
msgstr "Distingue maiúsculas de minúsculas:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, caseSensitiveCheck)
#: grepwidget.ui:176
#, fuzzy, kde-format
#| msgid "Case-sensitive searching."
msgctxt "@info:tooltip"
msgid "Case-sensitive searching."
msgstr "Buscar distinguindo as maiúsculas das minúsculas."

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: grepwidget.ui:189
#, fuzzy, kde-format
#| msgid "Location(s):"
msgctxt "@label:listbox"
msgid "Location(s):"
msgstr "Localización(s):"

#. i18n: ectx: property (toolTip), widget (KComboBox, filesCombo)
#: grepwidget.ui:202
#, fuzzy, kde-format
#| msgid "Files filter."
msgctxt "@info:tooltip"
msgid "Files filter"
msgstr "Filtro de ficheiros."

#. i18n: ectx: property (whatsThis), widget (KComboBox, filesCombo)
#: grepwidget.ui:205
#, fuzzy, kde-format
#| msgid ""
#| "Enter the file name pattern of the files to search here. You may give "
#| "several patterns separated by commas or spaces."
msgctxt "@info:whatsthis"
msgid ""
"Enter the file name pattern of the files to search here. You may give "
"several patterns separated by commas or spaces."
msgstr ""
"Escriba aquí o padrón dos nomes dos ficheiros que desexe buscar. Pode "
"indicar varios padróns separados por comas ou espazos."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: grepwidget.ui:218
#, fuzzy, kde-format
#| msgid "Exclude:"
msgctxt "@label:listbox"
msgid "Exclude:"
msgstr "Excluír:"

#. i18n: ectx: property (toolTip), widget (KComboBox, excludeCombo)
#: grepwidget.ui:231
#, fuzzy, kde-format
#| msgid "Files pattern to exclude"
msgctxt "@info:tooltip"
msgid "Files pattern to exclude"
msgstr "Padrón de ficheiros que excluír"

#. i18n: ectx: property (whatsThis), widget (KComboBox, excludeCombo)
#: grepwidget.ui:234
#, fuzzy, kde-format
#| msgid ""
#| "Enter the file name pattern of the files to exclude from the search here. "
#| "You may give several patterns separated by commas or spaces.<p>Every "
#| "pattern is internally surrounded by asterisks, so that each pattern can "
#| "match parts of the file paths.</p>"
msgctxt "@info:whatsthis"
msgid ""
"Enter the file name pattern of the files to exclude from the search here. "
"You may give several patterns separated by commas or spaces.<p>Every pattern "
"is internally surrounded by asterisks, so that each pattern can match parts "
"of the file paths.</p>"
msgstr ""
"Escriba aquí o padrón dos nomes dos ficheiros que desexe buscar. Pode "
"indicar varios padróns separados por comas ou espazos.<p>Cada padrón rodéase "
"internamente con asteriscos, polo que cada padrón pode coincidir con partes "
"das rutas aos ficheiros.</p>"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: grepwidget.ui:314
#, fuzzy, kde-format
#| msgid "Regular expression:"
msgctxt "@option:check"
msgid "Regular expression:"
msgstr "Expresión regular:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, regexCheck)
#: grepwidget.ui:327
#, fuzzy, kde-format
#| msgid "Enable or disable regular expression."
msgctxt "@info:tooltip"
msgid "Enable or disable regular expression."
msgstr "Activar ou desactivar as expresións regulares."

#. i18n: ectx: property (text), widget (QLabel, limitToProjectLabel)
#: grepwidget.ui:340
#, fuzzy, kde-format
#| msgid "Limit to project files:"
msgctxt "@option:check"
msgid "Limit to project files:"
msgstr "Límite aos ficheiros do proxecto:"

#. i18n: ectx: property (toolTip), widget (KComboBox, replacementTemplateEdit)
#: grepwidget.ui:369
#, fuzzy, kde-format
#| msgid "Enter the replacement template."
msgctxt "@info:tooltip"
msgid "Enter the replacement template"
msgstr "Introduza o modelo de substitución."

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: grepwidget.ui:376
#, fuzzy, kde-format
#| msgid "Replacement template:"
msgctxt "@label:textbox"
msgid "Replacement template:"
msgstr "Modelo de substitución:"

#. i18n: ectx: property (toolTip), widget (KComboBox, searchPaths)
#: grepwidget.ui:389
#, fuzzy, kde-format
#| msgid ""
#| "Select the location where you want to search. It can be a directory, a "
#| "file, or a semicolon separated (without spaces) list of directories/files."
msgctxt "@info:tooltip"
msgid ""
"Select the location where you want to search. It can be a directory, a file, "
"or a semicolon separated (without spaces) list of directories/files."
msgstr ""
"Escolla o lugar no que quere buscar. Pode ser un directorio, un ficheiro ou "
"unha lista separada por puntos e coma (sen espazos) de directorios ou "
"ficheiros."

#. i18n: ectx: property (toolTip), widget (QPushButton, directorySelector)
#: grepwidget.ui:414
#, fuzzy, kde-format
#| msgid "Select a directory to search in."
msgctxt "@info:tooltip"
msgid "Select a directory to search in"
msgstr "Escolle o directorio no que buscar."

#~ msgid "Find-Replace In Files"
#~ msgstr "Atopar ou substituír en ficheiros"

#~ msgctxt "@title:menu"
#~ msgid "Edit"
#~ msgstr "Editar"

#~ msgid "<big>%2 <i>(one match)</i></big>"
#~ msgid_plural "<big>%2 <i>(%1 matches)</i></big>"
#~ msgstr[0] "<big>%2 <i>(unha coincidencia)</i></big>"
#~ msgstr[1] "<big>%2 <i>(%1 coincidencias)</i></big>"

#, fuzzy
#~| msgid "Find/Replace in Files"
#~ msgid "Find/Replace"
#~ msgstr "Buscar ou substituír en ficheiros"

#~ msgid "&Find/Replace in Files"
#~ msgstr "B&uscar ou substituír en ficheiros"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr ""
#~ "Marce Villarino,\n"
#~ "Xosé Calvo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr ""
#~ "mvillarino@gmail.com,\n"
#~ "xosecalvo@gmail.com"

#~ msgid ""
#~ "Allows fast searching of multiple files using patterns or regular "
#~ "expressions. And allow to replace it too."
#~ msgstr ""
#~ "Permite facer unha procura rápida en varios ficheiros empregando patróns "
#~ "ou expresións regulares, e tamén realizar substitucións."

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Ctrl+Alt+f"
#~ msgstr "Ctrl+Alt+f"

#~ msgid "Synchronize with current document directory."
#~ msgstr "Sincronizar co directorio de documentos actual."

#~ msgid "&Change Search Settings"
#~ msgstr "&Cambiar a configuración da busca"

#~ msgid "Support for running grep over a list of files"
#~ msgstr "Soporte de execución de grep nunha lista de ficheiros"

#~ msgid "Recursive:"
#~ msgstr "Recursivo:"

#~ msgid "Find/Replace In Files"
#~ msgstr "Buscar ou substituír en ficheiros"

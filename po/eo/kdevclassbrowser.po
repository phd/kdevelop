# Translation of kdevclassbrowser into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdevclassbrowser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: classbrowserplugin.cpp:70 classbrowserplugin.cpp:121 classwidget.cpp:35
#, fuzzy, kde-format
#| msgid "Classes"
msgctxt "@title:window"
msgid "Classes"
msgstr "Klasoj"

#: classbrowserplugin.cpp:73
#, kde-format
msgctxt "@action"
msgid "Find in &Class Browser"
msgstr ""

#: classwidget.cpp:72
#, fuzzy, kde-format
#| msgid "S&earch:"
msgctxt "@label:textbox"
msgid "S&earch:"
msgstr "S&erĉi:"

#: classwidget.cpp:89
#, fuzzy, kde-format
#| msgid "Classes"
msgctxt "@info:whatsthis"
msgid "Class Browser"
msgstr "Klasoj"

#, fuzzy
#~| msgid "Classes"
#~ msgid "Base classes"
#~ msgstr "Klasoj"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Axel Rousseau"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "axel@esperanto-jeunes.org"
